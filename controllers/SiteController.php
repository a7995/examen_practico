<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\html;
use app\models\puerto;
use app\models\etapa;


class SiteController extends Controller
{
    
    
   public function actionConsulta1(){
       $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find('nompuerto', 'dorsal')
                        ->where(['altura' <1000]),
            
        ]);

        return $this->render('resultado', [
            'dataProvider' => $resultados,
            'campos'=>$campos=['nompuerto', 'dorsal'],
            'enunciado'=>$enunciado['Los puertos con altura menor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó']
        ]);
    }
       public function actionConsulta2(){
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find('numetapa', 'kms')
                        ->where(['salida' != 'llegada']),
            
            
        ]);

        return $this->render('resultado', [
            'dataProvider' => $resultados,
            'campos' => $campos=['numetapa', 'kms'],
            'enunciado'=>$enunciado['La etapas no circulares mostrando sólo el número de la etapa y la longitud de las mismas']
        ]);
    }
       public function actionConsulta3(){
       $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find('nompuerto', 'dorsal')
                        ->where(['altura' >1500]),
            
        ]);

        return $this->render('resultado', [
            'dataProvider' => $resultados,
            'campos' =>$campos=['nompuerto', 'dorsal'],
            'enunciado'=>$enunciado['Los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal que lo ganó']
            
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
